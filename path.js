const path = require('path')
const badpath = '/foo/bar//baz//bam/file.txt'
const pathstr1 = '/foo/bar/baz/bam/file.txt'
const pathstr2 = '/foo/bar'
const pathstr3 = '/whiz/wham'
const pathstr4 = '../whiz/wham'

console.log(path.basename(pathstr1)) //  'file.txt'

console.log(path.dirname(pathstr1)) //  '/foo/bar/baz/bam'

console.log(path.extname(pathstr1)) //  '.txt'

console.log(path.isAbsolute(pathstr1)) //  true
console.log(path.isAbsolute(pathstr4)) //  false

const pathObj = path.parse(pathstr1)
console.log(pathObj) /* outputs
  { root: '/',
    dir: '/foo/bar/baz/bam',
    base: 'file.txt',
    ext: '.txt',
    name: 'file'
  }
*/

console.log(path.format(pathObj)) //  '/foo/bar/baz/bam/file.txt'

console.log(path.normalize(badpath)) //  '/foo/bar/baz/bam/file.txt'

console.log(path.relative(pathstr1, pathstr2)) //  '../../..'
console.log(path.relative(pathstr2, pathstr1)) //  'baz/bam/file.txt'
console.log(path.relative(pathstr2, pathstr3)) //  '../../whiz/wham'

console.log(path.join(pathstr3, pathstr1)) // '/whiz/wham/foo/bar/baz/bam/file.txt'
console.log(path.join(pathstr1, pathstr3)) // '/foo/bar/baz/bam/file.txt/whiz/wham'

console.log(path.resolve(pathstr2, pathstr4)) // '/foo/whiz/wham'

console.log(path.delimiter) //  ':'

console.log(path.sep) //  '/'

/*
path.win32

{ resolve: [Function: resolve],
  normalize: [Function: normalize],
  isAbsolute: [Function: isAbsolute],
  join: [Function: join],
  relative: [Function: relative],
  _makeLong: [Function: _makeLong],
  dirname: [Function: dirname],
  basename: [Function: basename],
  extname: [Function: extname],
  format: [Function: format],
  parse: [Function: parse],
  sep: '\\',
  delimiter: ';',
  win32: [Circular],
  posix:
   { resolve: [Function: resolve],
     normalize: [Function: normalize],
     isAbsolute: [Function: isAbsolute],
     join: [Function: join],
     relative: [Function: relative],
     _makeLong: [Function: _makeLong],
     dirname: [Function: dirname],
     basename: [Function: basename],
     extname: [Function: extname],
     format: [Function: format],
     parse: [Function: parse],
     sep: '/',
     delimiter: ':',
     win32: [Circular],
     posix: [Circular] } }

*/
/*
path.posix

{ resolve: [Function: resolve],
  normalize: [Function: normalize],
  isAbsolute: [Function: isAbsolute],
  join: [Function: join],
  relative: [Function: relative],
  _makeLong: [Function: _makeLong],
  dirname: [Function: dirname],
  basename: [Function: basename],
  extname: [Function: extname],
  format: [Function: format],
  parse: [Function: parse],
  sep: '/',
  delimiter: ':',
  win32:
   { resolve: [Function: resolve],
     normalize: [Function: normalize],
     isAbsolute: [Function: isAbsolute],
     join: [Function: join],
     relative: [Function: relative],
     _makeLong: [Function: _makeLong],
     dirname: [Function: dirname],
     basename: [Function: basename],
     extname: [Function: extname],
     format: [Function: format],
     parse: [Function: parse],
     sep: '\\',
     delimiter: ';',
     win32: [Circular],
     posix: [Circular] },
  posix: [Circular] }
*/
