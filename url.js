const http = require('http')
const url =require('url')
const server = http.createServer()
const R = require('ramda')

server.on('request', (req, res) => {
  const urlObj = url.parse(`http://localhost:4000${req.url}`)

  res.writeHead(200, { 'Content-Type': 'text/html'})
  res.write(`
<pre>
${JSON.stringify(urlObj, null,4)}
</pre>

<a href="http://localhost:4000/">http://localhost:4000/</a>
<br />
<a href="http://localhost:4000/p/a/t/h">http://localhost:4000/p/a/t/h</a>
<br />
<a href="http://localhost:4000/?query=string">http://localhost:4000/?query=string</a>
<br />
<a href="http://localhost:4000/#hash">http://localhost:4000/#hash</a> -- Does not register as hash is not sent to the server.
<br />
<br />
Reference: http://user:pass@host.com:8080/p/a/t/h?query=string#hash
<br />
See: <a href="https://nodejs.org/dist/latest-v6.x/docs/api/url.html">https://nodejs.org/dist/latest-v6.x/docs/api/url.html</a>

`  , null, 4)
  res.end()
})

server.listen(4000)
require('open')('http://localhost:4000')
