const querystring = require('querystring');

const qString = 'foo=bar&abc=xyz&abc=123'
const qStringObj = querystring.parse(qString)
const backQString = querystring.stringify(qStringObj)

console.log('qString: ', qString) // foo=bar&abc=xyz&abc=123
console.log('qStringObj: ', qStringObj) // { foo: 'bar', abc: [ 'xyz', '123' ] }
console.log('backQString: ', backQString) // foo=bar&abc=xyz&abc=123
