const http = require('http')
const server = http.createServer()
const R = require('ramda')

server.on('request', (req, res) => {
  const omitProps = R.omit(['cookie'])

  const request = Object.keys(req) // req:ClientRequest
  const method = req.method
  const headers = omitProps(req.headers)
  const url = req.url
  const statusCode = req.statusCode
  const statusMessage = req.statusMessage


  res.writeHead(200, { 'Content-Type': 'text/plain'})
  res.write(JSON.stringify({ request, url, method, statusCode, statusMessage, headers, }, null, 4))
  res.end()
})

server.listen(4000)
require('open')('http://localhost:4000')
