const fs = require('fs')
const write_stream = fs.createWriteStream('./file.txt')
const net = require('net')

const server = net.createServer((socket) => {
  socket.pipe(write_stream)
}).listen(6000)

/*
`node tcp_stream_to_fs.js`
`telnet localhost 6000`
<type><enter>
<check hello.txt>
 */
