const net = require('net')
const fs = require('fs')

const server = net.createServer((socket) => {
  const read_stream = fs.createReadStream('file.txt')
  read_stream.pipe(socket)
}).listen(6001)

/*
`node tcp_stream_from_fs.js`
`telnet localhost 6001`

 */
